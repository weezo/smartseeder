<?php

namespace Weezo\SmartSeeder\Migrator;

use Illuminate\Database\Seeder as IluminateSeeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Seeder extends IluminateSeeder {


    protected $table = null;
    protected $sequencer = null;
    protected $beforeMaxId = null;

    public function __construct()
    {
        if($this->table !== null){
            $this->sequencer = $this->table.'_id_seq';
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){}

    /**
     * Before run the database seeds.
     *
     * @return void
     */
    public function beforeRun()
    {
        if($this->table !== null) {
            $beforeMaxId = DB::select("SELECT id FROM " . $this->table . " ORDER BY id DESC LIMIT 1");
            if(isset($beforeMaxId['0'])){
                $this->beforeMaxId = $beforeMaxId['0']->id;
            }
            else{
                $this->beforeMaxId = 0;
            }
        }

        Model::unguard();
    }

    /**
     * After run the database seeds.
     *
     * @param array $ids
     * @return void
     */
    public function afterRun($ids=[]){

        if($this->table !== null && $this->sequencer !== null)
        {
            $this->forceSetSequence($ids);
        }
    }

    /**
     * Before rollback the database seeds.
     *
     * @return void
     */
    public function beforeDown()
    {
        Model::unguard();
    }

    /**
     * After rollback the database seeds.
     *
     * @return void
     */
    public function afterDown(){}


    /**
     * Force the sequence update
     *
     * @param array $ids
     * @return int
     */
    public function forceSetSequence($ids=[]){

        if(is_array($ids) && !empty($ids)){

            if(DB::connection()->getName() == 'pgsql'){

                if($this->beforeMaxId < max($ids)){
                    $next_id = DB::select("select setval('".$this->sequencer."', ".max($ids).")");
                    return intval($next_id['0']->setval);
                }
                else{
                    return intval($this->beforeMaxId);
                }



            }

            return true;
        }

        return false;

    }


}