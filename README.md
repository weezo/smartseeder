# Weezo PKG SmartSeeder

Pacote para Laravel 5.2 desenvolvido por **Weezo**

> **EM DESENVOLVIMENTO**

> Pacote laravel para versionamento de seeds. Utiliza a mesma metodologia que é usado atualmente com as migrações. Desenvolvido com base no pacote [Laravel SmartSeeder](https://github.com/slampenny/SmartSeeder).


## Índice

* [Instalação](#instalação)
    * [Dependências](#1-dependência)
    * [Provider](#2-provider)
    * [Publicando arquivos](#3-publicando-arquivos)
    * [Migrations](#4-Publicando arquivos)
    * [Comandos](#5-comandos)
* [Desenvolvedores](#desenvolvedores)
* [Créditos](#créditos)

## Instalação

### 1. Dependência
Adicione o repositório e o require no `composer.json`:
```json
{
    "repositories":[
        {
            "type": "vcs",
            "url": "https://bitbucket.org/weezo/smartseeder"
        }
    ],
    "require": {
        "weezo/smart-seeder": "dev-master"
    }
}
```

### 2. Provider
Adicione o provider no arquivo `config/app.php`:
```php
'providers' => [
    Weezo\SmartSeeder\Providers\SmartSeederServiceProvider::class,
]
```

### 3. Publicando arquivos
Publique os arquivos do pacote executando o comando:
```shell
php artisan vendor:publish
php artisan optimize
```
> Será publicado: Configs e Migrations

### 4. Migrations
Execute as migrations, com o comando:
```shell
php artisan migrate
```

### 5. Comandos
| Comando | Descrição |
| --- | --- |
| `seed:run` | Executa todas as seed do diretório SmartSeeds que ainda não foram executadas |
| `seed:make` | Cria uma nova class de seeds mo ambiente especificado |
| `seed:rollback` | Rollback não excluir os registros do banco de dados (o que seria impossível com auto-incrementing primary key). O comando apenas permite re-executar o último lote de seeds |
| `seed:reset` | Rollback em todas as seeds. |
| `seed:refresh` | Reseta e executa todas as seeds |
| `seed:install` | Não precisa utilizar.... Esse comando será executado automaticamente quando outro comando for executado |
