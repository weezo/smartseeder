<?php

namespace Weezo\SmartSeeder\Providers;

use Illuminate\Support\ServiceProvider;
use App;
use Weezo\SmartSeeder\Commands\SeedRefreshCommand;
use Weezo\SmartSeeder\Commands\SmartSeederRepository;
use Weezo\SmartSeeder\Migrator\SeedMigrator;
use Weezo\SmartSeeder\Commands\SeedOverrideCommand;
use Weezo\SmartSeeder\Commands\SeedCommand;
use Weezo\SmartSeeder\Commands\SeedInstallCommand;
use Weezo\SmartSeeder\Commands\SeedMakeCommand;
use Weezo\SmartSeeder\Commands\SeedResetCommand;
use Weezo\SmartSeeder\Commands\SeedRollbackCommand;


class SmartSeederServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__ . '/../../Resources/config/smart-seeder.php' => config_path('smart-seeder.php')],'config');
        $this->publishes([__DIR__ . '/../../Resources/database/migrations/' => database_path('migrations')],'migrations');

        /*
        $this->publishes([__DIR__ . '/../../Resources/database/factories/' => database_path('factories')],'factories');
        $this->publishes([__DIR__ . '/../../Resources/database/seeds/' => database_path('seeds')],'seeds');
         */
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../Resources/config/smart-seeder.php', 'smart-seeder'
        );

        App::singleton('seed.repository', function($app) {
            return new SmartSeederRepository($app['db'], config('smart-seeder.seedTable'));
        });

        App::singleton('seed.migrator', function($app)
        {
            return new SeedMigrator($app['seed.repository'], $app['db'], $app['files']);
        });

        $this->app->bind('command.seed', function($app)
        {
            return new SeedOverrideCommand($app['seed.migrator']);
        });

        $this->app->bind('seed.run', function($app)
        {
            return new SeedCommand($app['seed.migrator']);
        });

        $this->app->bind('seed.install', function($app)
        {
            return new SeedInstallCommand($app['seed.repository']);
        });

        $this->app->bind('seed.make', function()
        {
            return new SeedMakeCommand();
        });

        $this->app->bind('seed.reset', function($app)
        {
            return new SeedResetCommand($app['seed.migrator']);
        });

        $this->app->bind('seed.rollback', function($app)
        {
            return new SeedRollbackCommand($app['seed.migrator']);
        });

        $this->app->bind('seed.refresh', function()
        {
            return new SeedRefreshCommand();
        });

        $this->commands([
            'seed.run',
            'seed.install',
            'seed.make',
            'seed.reset',
            'seed.rollback',
            'seed.refresh',
        ]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'seed.repository',
            'seed.migrator',
            'command.seed',
            'seed.run',
            'seed.install',
            'seed.make',
            'seed.reset',
            'seed.rollback',
            'seed.refresh',
        ];
    }



}